# Maintainer: slonkazoid <alifurkanyildiz@gmail.com>
# wlroots-git Maintainer: Adrian Perez de Castro <aperez@igalia.com>
# wlroots-git Maintainer: Antonin Décimo <antonin dot decimo at gmail dot com>
pkgname=wlroots-slonk-git
pkgver=0.17.0.r6559.72787db9
pkgrel=1
license=(custom:MIT)
pkgdesc="my wlroots pkgbuild"
url=https://gitlab.freedesktop.org/wlroots/wlroots
arch=(x86_64)
provides=("libwlroots.so" "wlroots=${pkgver%%.r*}" wlroots-git)
conflicts=(wlroots)
options=(debug)
depends=(
	glslang
	libinput
	libliftoff
	libxcb
	libxkbcommon
	opengl-driver
	pixman
	wayland
	xcb-util-errors
	xcb-util-renderutil
	xcb-util-wm
	seatd
	vulkan-icd-loader
	xorg-xwayland
	libdisplay-info)
makedepends=(
	git
	meson
	vulkan-headers
	wayland-protocols
	xorgproto)
source=("${pkgname}::git+${url}.git"
	"no_crash.patch"
	"revert-664ec590.patch")
sha512sums=('SKIP'
	'a81616b055e9852fdba131c903be8d7c8e1fadb3bf9eaaab6d546aab564f60254a71638b0f360e05b4f50d87b44ddf824ad5b1f65c20f029dfc1ab5a6f6bbcbc'
	'bde308d7844b79afdcdeb926e9a58b2b6c62eebdd45fd4ea2d3fd3b153febe0dc7e4932d2a428e73fd0905d37558ac86ae0411d2bf889ce27f5875f09f824303')

# For debugging
#CFLAGS="-pipe -march=x86-64 -mtune=generic -fno-omit-frame-pointer -fstack-protector-strong -fstack-clash-protection -fcf-protection"
#CXXFLAGS="${CFLAGS}"

_builddir="build"
_builddir_pkgver="build-pkgver"

_meson_setup () {
	arch-meson \
		--buildtype=debugoptimized \
		-Dwerror=false \
		-Dexamples=false \
		"${pkgname}" "$1"
}

prepare () {
	cd "${srcdir}/${pkgname}"
	#git clean -fd
	git reset --hard
	git apply ../no_crash.patch
	git apply ../revert-664ec590.patch
	cd "${srcdir}"
	_meson_setup "${_builddir_pkgver}"
}

pkgver () {
	(
		set -o pipefail
		meson introspect --projectinfo "${_builddir_pkgver}" \
		  | awk 'match($0, /"version":\s*"([[:digit:]]+\.[[:digit:]]+\.[[:digit:]]+)-dev"/, ret) {printf "%s",ret[1]}'
	)
	cd "${pkgname}"
	printf ".r%s.%s" "$(git rev-list --count HEAD)" "$(git rev-parse --short HEAD)"
}

build () {
	_meson_setup "${_builddir}"
	meson compile -C "${_builddir}"
}

package () {
	meson install -C "${_builddir}" --destdir="${pkgdir}"
	install -Dm644 "${pkgname}/"LICENSE "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
}

post_upgrade() {
  echo "Make sure to upgrade wlroots-slonk-git and sway-git together."
  echo "Upgrading one but not the other is unsupported."
}
